#!/usr/bin/env python3
import os
import argparse
import sys
import logging
import time
from string import Template

import yaml

############## CONSTANTS BLOCK BEGIN ################
LISTS_KEY = 'lists'
TEMPLATE_KEY = 'template'
EMAIL_KEY = "email"
VAR_LIST_KEY = "variables"
############## CONSTANTS BLOCK END ################

def add_timestamp_to_filename(filename:str) -> str:
    ts = time.localtime()
    timestamp = time.strftime("%Y-%m-%d-%H-%M-%S", ts)
    path, filename = os.path.split(filename)
    basename, ext = os.path.splitext(filename)
    result = f'{basename}-{timestamp}{ext}'
    return result

def init_logging():
    log_file_name = add_timestamp_to_filename(f'sendmail.log')
    logFileFormatter = logging.Formatter("%(asctime)s %(message)s")
    logConsoleFormatter = logging.Formatter("%(message)s")
    rootLogger = logging.getLogger()

    fileHandler = logging.FileHandler("{0}/{1}".format(".", log_file_name))
    fileHandler.setFormatter(logFileFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logConsoleFormatter)
    rootLogger.addHandler(consoleHandler)
    rootLogger.setLevel(logging.DEBUG)


def commandline_parser():
    LOG = logging.getLogger();
    parser = argparse.ArgumentParser(description='send mail helper')
    subparsers = parser.add_subparsers(dest='subcommand')
    # run test case parser
    send_subcommand = subparsers.add_parser('send')
    send_subcommand.add_argument('-l', '--list',
                                 action='store',
                                 dest='list_4_send',
                                 default='all',
                                 help='send mail according to email list inconfiguration file. default all')
    send_subcommand.add_argument('-c', '--config',
                                 action='store',
                                 dest='config_file',
                                 default="resources/config.yml",
                                 help='name of configuration file')
    # create email text n0 sending
    create_text_subcommand = subparsers.add_parser('create')
    create_text_subcommand.add_argument('-l', '--list',
                                        action='store',
                                        dest='list_4_send',
                                        help='send mail according to email list inconfiguration file. default all')
    create_text_subcommand.add_argument('-c', '--config',
                                        action='store',
                                        dest='config_file',
                                        help='name of configuration file')

    parser.add_argument('-v', '--version',
                        action='version',
                        version='Current Version of ' + os.path.basename(__file__) + ' : 0.0.2',
                        help='print current version')
    args = parser.parse_args(sys.argv[1:])
    LOG.debug(args)
    return args


def parse_config_file(config_file_name) -> dict:
    LOG = logging.getLogger();
    LOG.debug("We use : " + config_file_name)
    with open(config_file_name) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def sendmail(config : dict,  args):
    LOG = logging.getLogger();
    LOG.info("send e-mail according to configuration file : " + args.config_file)
    LOG.info("send e-mail according to list : " + args.list_4_send)
    config = parse_config_file(args.config_file)
    email_text_list = create_email_text(config, args)
    LOG.info("We have " + str(len(email_text_list)) + " emails")
    LOG.info("Real sending is not impelmented. So we use stubs")
    for text in email_text_list:
        LOG.info(text)


def create_email_text(config : dict, args) -> list:
    LOG = logging.getLogger();
    email_text_list : list = []
    list_dict = config[LISTS_KEY]
    if args.list_4_send not in list_dict:
        LOG.fatal("Incorrect mail list");
        sys.exit()
    list = list_dict[args.list_4_send]
    for reciepient_name in list:
        reciepient_desc = list[reciepient_name]
        template_fn = reciepient_desc[TEMPLATE_KEY]
        email_addr = reciepient_desc[EMAIL_KEY]
        template_content = read_template(template_fn)
        if VAR_LIST_KEY in reciepient_desc:
            vars_list = reciepient_desc[VAR_LIST_KEY]
            buff = Template(template_content)
            text = buff.substitute(vars_list)
            LOG.debug("prepare email to : " + reciepient_name + " according to template : " + template_fn + " with email : " + email_addr)
            LOG.debug(text)
            email_text_list.append(text)
        else:
            LOG.debug("prepare email to : " + reciepient_name + " according to template : " + template_fn + " with email : " + email_addr)
            LOG.debug(template_content)
            email_text_list.append(template_content)
    return email_text_list

def read_template(temlate_fn : str) -> list:
    LOG = logging.getLogger();
    LOG.debug("Use file " + temlate_fn)
    with open(temlate_fn) as f:
        data = f.read().replace('\n', '')
    return data

def main():
    init_logging()
    LOG = logging.getLogger();
    LOG.info("Sendmail started")
    args = commandline_parser()
    config = parse_config_file(args.config_file)
    if args.subcommand == 'send':
        create_email_text(config, args)
        sendmail(config, args)
    if args.subcommand == 'create':
        create_email_text(config, args)
    LOG.info("Sendmail completed")


if __name__ == '__main__':
    main()
